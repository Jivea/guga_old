param
(
  	$mode,
	$branch
)

function Main($mode, $branch)
{
	Output-Header "----- Continuous Integration -Mode: $($mode) ----"
	
	if($mode -eq "confirm")
	{
		Check-AllCommited
	
		Output-Info "Pull for changes (if any)"
		git pull origin master
	
		Build-Code
		Test-Code

		Output-Success "*** OK ***"
	}
	elseif($mode -eq "push")
	{
		Check-BranchSpecified $branch
		Check-AllCommited
	
		Output-Info "Pushing branch $($branch) to origin."
		git push origin $branch
	}
	elseif($mode -eq "promote")
	{
		Check-BranchSpecified $branch
		Check-AllCommited
		
		Output-Info "Switch to branch $($branch) and get master updates"
		git checkout $branch
		git pull
		git merge master --ff-only
		
		Build-Code
		Test-Code
		
		Output-Info "Switch to master and merge $($branch)"
		git checkout master
		git merge $branch --no-ff --log
		
		Output-Info "Publish merged master to origin"
		git push origin master
	}
	elseif($mode -eq "test")
	{
		Build-Code
		Test-Code
	}
	elseif($mode -eq "build")
	{
		Build-Code
	}
	else
	{
		Output-Error "Sorry, the specified mode doesn't exist."
	}
}
function Check-AllCommited()
{
	Output-Info "Check everything is commited: "
	$hasChanges = git status --porcelain
	if($hasChanges)
	{
		Output-Error "Not ok ! Working directory contains files to commit or ignore!"
		exit
	}
	Output-Success "OK"
}
function Check-BranchSpecified($branch)
{
	if(!$branch)
	{
		Output-Error "Missing branch parameter."
		exit
	}
}
function Build-Code()
{
	Output-Info "Build Code"
	& "./grunt" lint
	if($LastExitCode -ne 0)
	{
		Output-Error "Error encounterd while checking and building code ; request interrupted."
		exit -1
	}
}
function Test-Code()
{
	Output-Info "Test Code"
	& "./grunt" test
	if($LastExitCode -ne 0)
	{
		Output-Error "Error encounterd while performing tests ; request interrupted."
		exit -1
	}
}

function Output-Header($message)
{
	Write-Host $message -ForegroundColor Cyan
}
function Output-Info($message)
{
	Write-Host $message -ForegroundColor Magenta
}
function Output-Error($message)
{
	Write-Host $message -ForegroundColor Red
}
function Output-Success($message)
{
	Write-Host $message -ForegroundColor Green
}

Main $mode $branch


