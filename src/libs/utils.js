'use strict';

/*
 Returns a Buffer containing
 @param hex {string}

 */
exports.getBufferFromHexaString = function(hex) {
    var raw = new Buffer(hex.length/2); // 1 byte for every 2 character
    for (var i = 0; i < raw.length; i++) {
        var value = parseInt(hex.substr(i * 2, 2), 16);
        raw.writeUInt8(value, i);
    }
    return raw;
};