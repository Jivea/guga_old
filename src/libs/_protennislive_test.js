/**
 * Created by Jivea.
 * Testing protennislive client
 */

'use strict';

var assert = require('assert');
var client = require('./protennislive');

//exports.tearDown = function (done) {
//        done();
//};

exports.test_getCalendar = function (test) {
        client.getTournamentsCalendar(function(error, data) {
                test.ok(data, "Should have received data");
                test.ok(data.Calendar, "Expected one Calendar object in returned data");
                test.ok(data.Calendar.Tournament.length > 1, "Expected several tournaments in calendar");
                test.done();
        });
};