/**
 * Created by Jivea.
 * A client of protennislive.com API.
 */
'use strict';

var request = require('request');
var crypto = require('crypto');
var xml2js = require('xml2js');
var utils = require('./utils');


exports.getTournamentsCalendar = function(callback) {
    request('http://ws.protennislive.com/LiveScoreSystem/F/Long/GetCalendarCrypt.aspx', function (error, response, body) {
        if(error) {
            callback(error, null);
        }
        else if(response.statusCode === 200) {
            var decryptedXml = decryptXml(body);
            xml2js.parseString(decryptedXml, function(error, result){
                if(error){
                    callback(error, null);
                }
                else {
                    callback(null, result);
                }
            });
        }
        else {
            callback(null, null);
        }
    });
};

function decryptXml(xml){
    var data = new Buffer(xml, "base64");
    var cipherType = "aes-256-cbc";
    var cipherKey = utils.getBufferFromHexaString('fc74aa22c4930872b0adc27a2ad8fe4e49cbe003ef09418e0dad03ba5e2132f8');
    var iv = data.slice(0, 16); // 16 first bytes of data is the IV
    data = data.slice(16);


    var decipher = crypto.createDecipheriv(cipherType, cipherKey, iv);
    var result = decipher.update(data, 'buffer', 'utf8');
    result += decipher.final('utf8');
    return result;
}



