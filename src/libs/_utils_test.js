/**
 * Created by Jivea.
 * testing the util.js module
 */

'use strict';
var util = require("./utils.js");
var assert = require("assert");

exports.test_getBufferFromHexaString = function (test) {
    var buffer = util.getBufferFromHexaString("FF");
    test.equals(255, buffer.readUInt8(0), "Returned value should have been equal to 255");
    test.done();
};

exports.test_getBufferFromHexaString = function (test) {
    var buffer = util.getBufferFromHexaString("fc74aa22c4930872b0adc27a2ad8fe4e49cbe003ef09418e0dad03ba5e2132f8");
    test.equals(252, buffer.readUInt8(0), "unexpected value at index 0 of buffer");
    test.equals(116, buffer.readUInt8(1), "unexpected value at index 1 of buffer");
    test.equals(170, buffer.readUInt8(2), "unexpected value at index 2 of buffer");
    test.equals(8, buffer.readUInt8(6), "unexpected value at index 6 of buffer");
    test.equals(248, buffer.readUInt8(31), "unexpected value at index 32 of buffer");
    test.done();
};
