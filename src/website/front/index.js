/**
 * Created by Jivea
 * index.js
 */

/*global $, renderer*/
var currentDate = new Date();

$(function(){
    "use strict";

    loadTournaments(currentDate);

    $("#btnBefore").on("click", function(){
        currentDate.setDate(currentDate.getDate()-7);
        loadTournaments(currentDate);

    });

    $("#btnAfter").on("click", function(){
        currentDate.setDate(currentDate.getDate()+7);
        loadTournaments(currentDate);
    });

});

function loadTournaments(date) {
    "use strict";

    var formatedDate = formatDate(date);
    $.get("/api/tournaments/"+formatedDate,function(data,status){
        if(status==="success") {
            onTournamentsLoaded(data);
        }
    });
}
function onTournamentsLoaded(data){
    "use strict";

    var placeHolder = $("#tournamentList");
    placeHolder.empty();

    var tournamentList = data.data;
    for(var i =0 ; i<tournamentList.length ; i++){
        placeHolder.append(renderer.renderTournament(tournamentList[i]));
    }
}

function formatDate(date) {
    "use strict";

    var dd = date.getDate();
    var mm = date.getMonth()+1;
    var yyyy = date.getFullYear();

    if(dd<10) {
        dd='0'+dd;
    }
    if(mm<10) {
        mm='0'+mm;
    }
    return yyyy+"-"+mm+"-"+dd;
}

