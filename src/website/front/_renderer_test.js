/*global suite, test, assert, renderer */
(function() {
    "use strict";

    suite("Example", function () {
        test("should run", function () {
            assert.equal(renderer.foo(), 'foo','foo equal `bar`');
        });
    });
}());
