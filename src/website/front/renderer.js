/**
 * Created by Jivea
 * renderer.js
 * + Renders data to html items
 */
var renderer = {};
(function() {
    "use strict";
    renderer.renderTournament = renderTournament;
    renderer.foo = function(){
        return "foo";
    };
}());

function renderTournament(tournament) {
    "use strict";

    var row = "<tr>"  +
                "<td>" + tournament.name + "</td>" +
                "<td>" + tournament.title + "</td>" +
                "<td>" + tournament.category + "</td>" +
                "<td>" + tournament.year + "</td>" +
                "<td>" + tournament.startDate + "</td>" +
                "<td>" + tournament.endDate + "</td>" +
                "<td>" + tournament.location + "</td>" +
                "<td>" + tournament.prize + "</td>" +
                "<td>" + tournament.surface + "</td>";
    if(tournament.indoor)
        row +="<td>Indoor</td>";
    else
        row +="<td></td>";
    row += "</tr>";
    return row;
}