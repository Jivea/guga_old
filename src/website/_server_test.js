/**
 * Created by Jivea.
 * testing the server.js class
 */

"use strict";
var PORT = 8083;
var GENERATED_FOLDER = "generated";
var GENERATED_HOMEPAGE = "test/pageHome.html";
var GENERATED_404PAGE = "test/page404.html";

var server = require("./server.js");
var http = require("http");
var fs = require("fs");
var assert = require("assert");
var domain = require("./back/domain/domain.js");

exports.tearDown = function (done) {

    cleanUp(GENERATED_FOLDER + "/" + GENERATED_HOMEPAGE);
    cleanUp(GENERATED_FOLDER + "/" + GENERATED_404PAGE);

    if (server.isStarted()) {
        server.stop(function () {
            done();
        });
    } else {
        done();
    }
};

exports.test_servesHomePageWhenRootUrl = function (test) {
    var fileContent = "Hello World";
    fs.writeFileSync(GENERATED_FOLDER + '/' + GENERATED_HOMEPAGE, fileContent);

    httpGet("http://localhost:" + PORT, function (response, responseData) {
        test.equals(200, response.statusCode, "Returned status code is not correct");
        test.equals(fileContent, responseData, "response content not OK");
        test.ok(responseData, "Should have received data");
        test.done();
    });
};

exports.test_serves404PageWhenRandomUrl = function (test) {
    var fileContent = "This is the 404";
    fs.writeFileSync(GENERATED_FOLDER + '/' + GENERATED_404PAGE, fileContent);

    httpGet("http://localhost:" + PORT + "/random", function (response, responseData) {
        test.equals(404, response.statusCode, "Returned status code is not correct");
        test.equals(fileContent, responseData, "response content not OK");
        test.done();
    });
};

exports.test_servesSpecificPage = function (test) {
    var fileContent = "Hello World";
    fs.writeFileSync(GENERATED_FOLDER + '/' + GENERATED_HOMEPAGE, fileContent);

    httpGet("http://localhost:" + PORT + "/" + GENERATED_HOMEPAGE, function (response, responseData) {
        test.equals(200, response.statusCode, "Returned status code is not correct");
        test.equals(fileContent, responseData, "response content not OK");
        test.done();
    });
};

exports.test_stopCallsTheCallback = function (test) {
    server.start(GENERATED_FOLDER, GENERATED_HOMEPAGE, GENERATED_404PAGE, PORT, function () {
        server.stop(function () {
            test.done();
        });
    });
};

exports.test_stopThrowsExceptionWhenServerIsNotRunning = function (test) {
    test.throws(function () {
        server.stop();
    });
    test.done();
};

exports.test_isStartedFollowsServerState = function (test) {
    test.ok(!server.isStarted());

    server.start(GENERATED_FOLDER, GENERATED_HOMEPAGE, GENERATED_404PAGE, PORT, function () {
        test.ok(server.isStarted(), "isStarted should return true");

        server.stop(function () {
            test.ok(!server.isStarted(), "isStarted should return false");
            test.done();
        });
    });
};

exports.test_getCurrentTournaments = function (test) {
    httpGet("http://localhost:" + PORT + "/api/tournaments/2014-11-11", function (response, responseString) {
        test.equals(200, response.statusCode, "Returned status code is not correct");

        var responseData = JSON.parse(responseString);
        var tournamentList = responseData.data;

        test.ok(tournamentList, "Should have received data");
        test.ok(tournamentList.length > 1, "Should have more than one tournament");
        test.ok(tournamentList[0].name, "First tournament should have a name");
        test.done();
    });
};

function httpGet(url, callback) {
    server.start(GENERATED_FOLDER, GENERATED_HOMEPAGE, GENERATED_404PAGE, PORT, function () {
        http.get(url, function (response) {
            var receivedData = "";
            response.setEncoding("utf8");

            response.on("data", function (chunk) {
                receivedData = chunk;
            });

            response.on("end", function () {
                callback(response, receivedData);
            });
        });
    });
}

function cleanUp(file) {
    if (fs.existsSync(GENERATED_HOMEPAGE)) {
        fs.unlinkSync(GENERATED_HOMEPAGE);
        assert.ok(!fs.existsSync(GENERATED_HOMEPAGE), "Could not delete file:" + GENERATED_HOMEPAGE);
    }
}