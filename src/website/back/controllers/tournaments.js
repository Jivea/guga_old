/**
 * Created by Jivea
 * Tournaments Controller
 * + Answers api web requests
 */
'use strict';

var model = require('../models/tournaments');
var moment = require('moment');

exports.getTournaments = function(req, res, next){
    var date = moment(req.param('date'), 'YYYY-MM-DD');
    model.get(date, function(err, data){
        res.status(err ? 503 : 200).json({
            error: err ? true : null,
            errorMessage: err ? err : null,
            data: data
        });
    });
};