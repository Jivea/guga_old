/**
 * Created by Jivea
 * Tournaments Model
 * + Provides controllers with data
 */
'use strict';
var moment = require('moment');
var protennislive = require('../../../libs/protennislive.js');

var Tournament = require('../domain/domain.js').Tournament;

var dateStr ;
exports.get = function(date, callback){
    dateStr = date.format("YYYY-MM-DD");
    protennislive.getTournamentsCalendar(function(error, data) {
        if(error) {
            callback(error, null)  ;
        }
        else{
            var currentTournaments = data.Calendar.Tournament.filter(isCurrent)
                .map(mapProtennisliveTournamentToDomainTournament);
            callback(null, currentTournaments);
        }
    });
};

function isCurrent(protennisliveTournament) {
    return protennisliveTournament.$.sDate <= dateStr && protennisliveTournament.$.eDate >= dateStr;
}
function mapProtennisliveTournamentToDomainTournament(protennisliveTournament) {
    return new Tournament(
        protennisliveTournament.$.id,
        protennisliveTournament.$.title,
        protennisliveTournament.$.name,
        protennisliveTournament.$.tour,
        protennisliveTournament.$.type,
        protennisliveTournament.$.year,
        protennisliveTournament.$.sDate,
        protennisliveTournament.$.eDate,
        protennisliveTournament.$.loc,
        protennisliveTournament.$.prize,
        protennisliveTournament.$.sfc,
        protennisliveTournament.$.indoor,
        protennisliveTournament.$.sDraw,
        protennisliveTournament.$.dDraw,
        protennisliveTournament.$.sWin,
        protennisliveTournament.$.dWin1,
        protennisliveTournament.$.dWin2);
}
