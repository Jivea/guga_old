/**
 * Created by Jivea
 * Domain
 * + Holds domain objects
 */
'use strict';

exports.Tournament = function Tournament(   id,
                                            title,
                                            name,
                                            tour,
                                            type,
                                            year,
                                            sDate,
                                            eDate,
                                            loc,
                                            prize,
                                            sfc,
                                            indoor,
                                            sDraw,
                                            dDraw,
                                            sWin,
                                            dWin1,
                                            dWin2) {

    this.id = id;
    this.title = title;
    this.name = name;
    this.tour = tour;
    this.category = type;
    this.year = year;
    this.startDate = sDate;
    this.endDate = eDate;
    this.location = loc;
    this.prize = prize;
    this.surface = sfc;
    this.indoor = indoor;
    this.singleDrawSize = sDraw;
    this.doubleDrawSize = dDraw;
    this.singleWinner = sWin;
    this.doubleWinner1 = dWin1;
    this.doubleWinner2 = dWin2;
};
