/**
 * Created by Jivea
 * Launch the Guga server.
 */
(function(){
    "use strict";

    var server = require("./src/website/server.js");
    var port =  process.env.PORT || 1337;
    server.start("./src/website/frontEnd","index.html", "404.html", port, function(){
        console.log("Server started");
    });
})();