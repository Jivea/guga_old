module.exports = function(grunt) {
    // Project configuration.
    grunt.initConfig({
        watch: {
            jshint: {
                files: frontEndFiles().concat(nodeFiles()),
                tasks: ['jshint']
            },
            karma: {
                files: frontEndFiles(),
                tasks: ['karma:coding:run']
            }
        },
        jshint: {
            node: {
              src: nodeFiles() ,
              options: nodeLintOptions()
            },
            frontEnd: {
                src: frontEndFiles(),
                options: globalLintOptions()
            }
        },
        nodeunit: {
            all: [  "src/website/back/_*_test.js",
                    "src/libs/**/_*_test.js"]
        },
        karma: {
            options: {
                configFile: 'config/karma.conf.js',
                browsers: [/*'IE',*/ 'PhantomJS', /*'Chrome',*/ 'Firefox'],
                files: [
                    "external/jquery-2.1.1.min.js", // jquery to be sent to the browser
                    "node_modules/chai/chai.js",    // chai.js to be sent to the browser
                    "config/config-mocha-karma.js",        // use mocha tdd style
                ].concat(frontEndFiles())
            },
            coding: codingKarmaOptions(),
            build: buildKarmaOptions()
        }
    });

    // Load grunt plugins
    grunt.loadNpmTasks("grunt-contrib-jshint");
    grunt.loadNpmTasks("grunt-contrib-nodeunit");
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-karma');

    // Register tasks
    grunt.registerTask("default", "Lint & test", ["jshint", "test"]);
    grunt.registerTask("lint", "Lint", ["jshint"]);
    grunt.registerTask("test", "Test everything", ["nodeunit", "karma:build"]);

    grunt.registerTask("livecoding", "Triggers karma and jshint on file change (must run command: grunt livecoding watch)", ["karma:coding:start", "jshint"]);


    function globalLintOptions() {
        return {
            bitwise: true,
            curly: false,
            eqeqeq: true,
            forin: true,
            immed: true,
            latedef: false,
            newcap: true,
            noarg: true,
            noempty: true,
            nonew: true,
            regexp: true,
            undef: true,
            strict: true,
            trailing: true
        };
    }
    function nodeLintOptions() {
        var options = globalLintOptions();
        options.node = true;
        return options;
    }

    function globalKarmaOptions() {
        return {
            configFile: 'karma.conf.js',
            browsers: ['IE', 'PhantomJS', 'Chrome', 'Firefox'],
            files: ["src/website/front/*.js"]/*frontEndFiles().concat([
                'node_modules/chai/chai.js'
            ])*/
        };
    }
    function codingKarmaOptions() {
        return {
            background:true
        };
        //var options = globalKarmaOptions();
        //options.background = true;
        //return options;

    }
    function buildKarmaOptions() {
        return {
            singleRun: true,
            browserNoActivityTimeout: 2000
        };
        //var options = globalKarmaOptions();
        //options.singleRun = true;
        //options.browserNoActivityTimeout=2000;
        //return options;
    }

    function frontEndFiles(){
        return ["src/website/front/*.js"];
    }
    function nodeFiles() {
        return [    "Gruntfiles.js",
                    "src/website/back/*.js",
                    "src/libs/**/*.js"];
    }
};